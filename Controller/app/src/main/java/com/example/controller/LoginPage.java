package com.example.controller;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class LoginPage extends AppCompatActivity {

    Button bVerif;
    TextView tUser;
    TextView tPass;
    String cUser = "Paul";
    String cPass = "Oui";
    String dUser = "Jonathan";
    String dPass = "Non";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_page);
        tUser = (TextView) findViewById(R.id.eTLogin);
        tPass = (TextView) findViewById(R.id.eTPass);
        bVerif = (Button) findViewById(R.id.bLogin);

        bVerif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user = tUser.getText().toString();
                String pass = tPass.getText().toString();

                if (user.equals(cUser) && pass.equals(cPass)) {
                    Intent gameActivity = new Intent(LoginPage.this, MainActivity.class);
                    startActivity(gameActivity );

                } else {
                    Toast.makeText(LoginPage.this, "Wrond username or password", Toast.LENGTH_LONG).show();
                }
                if (user.equals(dUser) && pass.equals(dPass)) {
                    Intent gameActivity = new Intent(LoginPage.this, MainActivity.class);
                    startActivity(gameActivity);
                }
            }
        });
    }
}
